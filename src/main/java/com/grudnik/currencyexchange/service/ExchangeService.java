package com.grudnik.currencyexchange.service;

import com.grudnik.currencyexchange.model.Record;
import com.grudnik.currencyexchange.model.Type;
import com.grudnik.currencyexchange.repository.RecordRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;
import java.util.Date;


@Service
public class ExchangeService {


    @Autowired
    private RecordRepository recordRepository;

    public String getAvailableCurrencies() throws IOException, JSONException {


        URL urlws = new URL("http://api.nbp.pl/api/exchangerates/tables/A?format=json");
        URLConnection tc = urlws.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                tc.getInputStream()));

        String s;
        s = in.readLine();
        JSONArray jsonArray = new JSONArray(s);
        String jsonString = jsonArray.get(0).toString();
        JSONObject jsonObject = new JSONObject(jsonString);
        jsonArray = (JSONArray) jsonObject.get("rates");

        JSONArray myJsonArray = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject myJsonObject = new JSONObject();
            myJsonObject.put("code", jsonArray.getJSONObject(i).get("code"));
            myJsonObject.put("name", jsonArray.getJSONObject(i).get("currency"));
            myJsonArray.put(myJsonObject);
        }
        Record r = new Record(Type.LIST, new Date());
        recordRepository.save(r);
        return myJsonArray.toString();
    }


    public String calcuateCurrency(double amount, String from, String to) throws IOException, JSONException {

        double fromRate = getRateByCode(from);
        double toRate = getRateByCode(to);

        double x = (amount / toRate) * fromRate;
        x = x * 100;
        x = Math.round(x);
        x = x / 100;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("beforAmount", amount);
        jsonObject.put("beforCurrency", from);
        jsonObject.put("afterAmount", x);
        jsonObject.put("afterCurrency", to);


        Record r = new Record(Type.CALCULATE, new Date());
        recordRepository.save(r);
        return jsonObject.toString();
    }


    public String getRatesByList(JSONObject jsonObject) throws JSONException, IOException {

        JSONArray jsonArray = jsonObject.getJSONArray("curr");

        JSONArray myJsonArray = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {

            String code = jsonObject.getJSONArray("curr").get(i).toString();
            JSONObject myJsonObject = new JSONObject();
            myJsonObject.put("code", code);
            myJsonObject.put("rate", getRateByCode(code));
            myJsonArray.put(myJsonObject);
        }

        Record r = new Record(Type.RATES, new Date());
        recordRepository.save(r);
        return myJsonArray.toString();
    }

    private double getRateByCode(String code) throws IOException, JSONException {
        URL urlws = new URL("http://api.nbp.pl/api/exchangerates/rates/A/" + code + "/?format=json");
        URLConnection tc = urlws.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                tc.getInputStream()));

        String s;
        s = in.readLine();

        JSONObject jsonObject = new JSONObject(s);
        JSONArray jsonArray = (JSONArray) jsonObject.get("rates");
        jsonObject = new JSONObject(jsonArray.get(0).toString());

        return jsonObject.getDouble("mid");

    }


}
