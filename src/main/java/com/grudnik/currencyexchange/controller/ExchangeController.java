package com.grudnik.currencyexchange.controller;

import com.grudnik.currencyexchange.service.ExchangeService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class ExchangeController {
    @Autowired
    private ExchangeService exchangeService;

    @GetMapping(value = "/list")
    public String getAvailableCurrencies() throws IOException, JSONException {

        return exchangeService.getAvailableCurrencies();
    }

    @PostMapping(value = "/list/rates")
    public String getAvailableCurrenciesWithRates(HttpServletRequest request) throws IOException, JSONException {
        String requestData = request.getReader().lines().collect(Collectors.joining());
        JSONObject obj = new JSONObject(requestData);
        return exchangeService.getRatesByList(obj);
    }


    @GetMapping(value = "/calculate/{amount}/{from}/{to}")
    public String calcuateCurrency(@PathVariable double amount, @PathVariable String from, @PathVariable String to) throws IOException, JSONException {

        return exchangeService.calcuateCurrency(amount, from, to);


    }


}
