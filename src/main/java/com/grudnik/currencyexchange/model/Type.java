package com.grudnik.currencyexchange.model;

public enum Type {
    LIST,
    CALCULATE,
    RATES
}
